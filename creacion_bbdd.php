<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<head>
        <meta charset="UTF-8">
        <title>Creando Base de Datos</title>
</head>
<body>
<?php
$servername = "localhost";
$username = "root";
$password = "";

$conn = mysqli_connect($servername, $username, $password);
$del_bbdd = "DROP DATABASE IF EXISTS form;";
$bbdd = "CREATE DATABASE form;";
$use_bbdd = "USE form;";
$tabla = "CREATE TABLE usuario
    (
        id INT AUTO_INCREMENT,
        imagen LONGBLOB,
        nombre VARCHAR(25),
        apellido VARCHAR(70),
        sexo ENUM('Masculino', 'Femenino'),
        nacimiento DATE,
        mail VARCHAR(50),
        telefono CHAR(9),
        direccion VARCHAR(45),
        provincia VARCHAR(30),
        pais_nacimiento VARCHAR(45),
        usuario VARCHAR(30),
        pass VARCHAR(50),
        ciclo VARCHAR(25),
        modulo VARCHAR(55),
        preferencia VARCHAR(40),
        linkedin VARCHAR(90),
        ingles ENUM ('Malo', 'Regular', 'Bueno'),
        PRIMARY KEY (id)
    ); ";

mysqli_query($conn,$del_bbdd);
if (mysqli_query($conn, $bbdd) ) {
    mysqli_query($conn, $use_bbdd);
    if (mysqli_query($conn, $tabla)) {
        echo "Base de datos creada";
    }
    else {
        echo "error: " . mysqli_error($conn);
    }
}
else {
    echo "Error de creación " . mysqli_error($conn);
}
if (!$conn) {
    die("Conexion fallida: ". mysqli_connect_error());
}

mysqli_close($conn);
?>
</body>
</html>
